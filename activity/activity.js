// data
db.users.insertMany([
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
        courses: [ "CSS", "Javascript", "Python" ],
        department: "HR"
    },
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 50,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    }
]);

//2.  Find users with letter s in their first name or d in their last name.
// - Use the $or operator.
//- Show only the firstName and lastName fields and hide the _id field.

db.users.find( {$or: [ { firstName: { $regex: 's', $options: '$i'}}, { lastName: { $regex: 'd', $options: '$i'}} ] }, {
    firstName: 1,
    lastName: 1,
    _id: 0  
} );

// simplify mode
db.users.find(
    { $or: [
       { firstName: { $regex: /j/i } },
       { lastName: { $regex: /d/i } }
    ] },
    { firstName: 1, lastName: 1, _id: 0 }
 )


/* 3. Find users who are from the HR department and their age is greater than or equal to 70.
- Use the $and operator */

db.users.find( { $and: [ { department: "HR" }, { age : {$gte: 70} } ] } );


/* 4. Find users with the letter e in their first name and has an age of less than or equal to 30.
- Use the $and, $regex and $lte operators */
db.users.find( { $and: [ { firstName: { $regex: 'e', $options: '$i'} }, { age : {$lte: 30} } ] } );

