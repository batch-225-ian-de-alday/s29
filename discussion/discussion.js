// MongoDB - Query Operators and field projections

// Inserting data through our database
// using Many
db.users.insertMany([
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
        courses: [ "CSS", "Javascript", "Python" ],
        department: "none"
    },
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 50,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "none"
    }
]);

//===========================================

// [SECTION] Comparison Query Operators

// $gt/$gte operator
// - Allows us to find documents that have field number values greater than or equal to a specified value.

db.users.find({ age : { $gt: 50}});
db.users.find({ age : { $gte: 50}});

// $lt/$lte operator
// - Allows us to find documents that have field number values less than or equal to a specified value.

db.users.find({ age : { $lt: 50}});
db.users.find({ age : { $lte: 50}});


// $ne operator
// - Allows us to find documents that have field number values not equal to a specified value.

db.users.find({ age : { $ne: 82}});


// $in operator

/*    - allows us to find documents with specific match criteria one field using different values.
    - syntax
     db.collectionName.find({field : { $in : value }}); */

db.users.find( { lastName: { $in: ["Hawking", "Doe"]}});
db.users.find( { courses: { $in: ["HTML", "React"]}});

// =====================================

// [SECTION] Logical Query Operators

// $or operator

/* 
    - Allows us to find documents that match a single criteria from multiple provided search criteria.
	- Syntax
		db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/

// this will display the firstName if it has neil or none only. and age with 50 or none. like 1 or 0 using OR operator
db.users.find( {$or: [ { firstName: "Neil"}, { age : 50 } ] } );
// this will display the firstName if it has neil or none only. and age with greater done 50 or none. like 1 or 0 using OR operator
db.users.find( {$or: [ { firstName: "Neil"}, { age : { $gt: 30} } ] } );


// $and operator
/* 
    - Allows us to find documents matching multiple criteria in a single field.
*/
// this will display all the object with age that is not equal to 82 and 50.
db.users.find( { $and: [ { age: { $ne: 82} }, { age : {$ne: 50} } ] } );

db.users.find({ $and: [ { age : { $ne: 82 } }, { age : { $ne: 50 } }] });

// ======================================

// [SECTION] Field Projection
/* 
    - Retrieving documents are common operators that we do and by default MongoDB queries return the whole document as a response
    - When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
    - To help with readability of the values returned, we can include/exclude fields from the response.
*/

// Inclusions

/* 
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 1 or true to denote that the field is being included
    - Syntax:
        db.collectionName.find({criteria}, {field: 1}) 1 or true can be used in field
*/

db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        contact: 1,
    }
);

// inclusion of object phone only in an object
db.users.find(
    {
        firstName: "Stephen"
    },
    {
        firstName: 1,
        lastName: 1,
        "contact.phone": 1
    }
);

// Exclusion
/* 
    - Allows us to exclude/remove specific fields only when retrieving documents.
    - The value provided is 0 or false to denote that the field is being included
    - Syntax:
        db.collectionName.find({criteria}, {field: 0}) 0 or false can be used in field
*/

db.users.find(
    {
        firstName: "Stephen"
    },
    {
        contact: 0,
        course: 0
    }
);

//exclusion of object phone only in an object
db.users.find(
    {
        firstName: "Stephen"
    },
    {
        "contact.phone": 0
    }
);


// _id exclusion is the only thing that we can exclude while using inclussion
db.users.find(
    {
        firstName: "Stephen"
    },
    {
        "contact.phone": 1,
        _id: 0
        
    }
);


//======================================

// [SECTION] Evaluation Query Operators

// $regex Operator
/* 
    - Allows us to find documents that match a specific string patter using regular expressions.
    - $options: '$i' - is for Case insensitivity to match upper and lower cases.
    - $options: '$x" - "Extended" capability to ignore all white space characters in the $regex pattern unless escaped or included in a character class
*/

// Case sensitive query
db.users.find({ firstName: { $regex: 'N'} } );


// Case insensitive query
db.users.find({ firstName: { $regex: 'j', $options: '$i'} } );

//using $options: '$x'
db.users.find({firstName: { $regex: 'o', $options: '$x'}});





// regex and inclusion sample
db.users.find({ firstName: { $regex: 'j', $options: '$i'} }, {
    "contact.phone": 1,
    firstName: 1,
    _id: 0
    
} );